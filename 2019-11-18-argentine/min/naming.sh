#!/bin/bash

for fname in *.jpg; do
	num=${fname#"photo-"}
	num=${num%".jpg"}
	nnum=$(printf "%03d" $num)
	echo "$fname -> photo-$nnum.jpg"
	mv $fname photo-$nnum.jpg
done
